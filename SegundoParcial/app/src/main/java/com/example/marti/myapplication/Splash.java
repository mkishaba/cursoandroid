package com.example.marti.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

public class Splash extends AppCompatActivity {
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mProgressBar = findViewById(R.id.progressBar);
        SplashAsyncTask splashTask;
        splashTask = new SplashAsyncTask(mProgressBar);
        splashTask.execute();
    }

    public class SplashAsyncTask extends AsyncTask<Void, Integer, Boolean> {
        private android.widget.ProgressBar mProgressBar;

        public SplashAsyncTask(android.widget.ProgressBar progressBar) {
            mProgressBar = progressBar;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            int i;
            for (i=0; i<2000; i++) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return false;
                }
                publishProgress(i);
            }
            publishProgress(i);
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values){
            mProgressBar.setProgress(values[0]);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setMax(2000);
            mProgressBar.setProgress(0);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Intent intent = new Intent(Splash.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}

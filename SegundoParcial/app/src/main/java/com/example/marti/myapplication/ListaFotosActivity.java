package com.example.marti.myapplication;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ListaFotosActivity extends AppCompatActivity {
    private RecyclerView mListaFotos;
    private FirebaseRecyclerAdapter<Detalle, DetalleViewHolder> mListaFotosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_fotos);

        Intent intent = getIntent();
        String clave = intent.getStringExtra("@string/tipo_vehiculo");

        Query query = FirebaseDatabase.getInstance().getReference().child(clave);
        FirebaseRecyclerOptions<Detalle> options = new FirebaseRecyclerOptions.Builder<Detalle>()
                .setQuery(query, Detalle.class).build();

        mListaFotosAdapter = new FirebaseRecyclerAdapter<Detalle, DetalleViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final DetalleViewHolder holder, final int position, @NonNull Detalle model) {
                holder.marca.setText(model.getMarca());
                holder.modelo.setText(model.getModelo());
                holder.downloadProgressBar.setVisibility(View.VISIBLE);
                Glide.with(ListaFotosActivity.this)
                        .load(model.getPath_imagen())
                        .listener(new RequestListener<Drawable>() {
                          @Override
                          public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.downloadProgressBar.setVisibility(View.GONE);
                            return false;
                          }

                          @Override
                          public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.downloadProgressBar.setVisibility(View.GONE);
                            return false;
                          }
                        })
                        .into(holder.foto);
            }

            @NonNull
            @Override
            public DetalleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.view_holder2_layout, parent, false);
                return new DetalleViewHolder(view);
            }
        };

        mListaFotos = findViewById(R.id.listaFotos);
        mListaFotos.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        mListaFotos.setAdapter(mListaFotosAdapter);
        mListaFotos.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        mListaFotos.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mListaFotosAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mListaFotosAdapter.stopListening();
    }
}

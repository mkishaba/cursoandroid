package com.example.marti.myapplication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private FirebaseRecyclerAdapter<Vehiculo, VehiculoViewHolder> mAdapter;
    private ProgressBar mMainProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainProgressBar = findViewById(R.id.mainProgressBar);
        mMainProgressBar.setVisibility(View.GONE);
        Query query = FirebaseDatabase.getInstance().getReference().child("Vehiculos");

        FirebaseRecyclerOptions<Vehiculo> options = new FirebaseRecyclerOptions.Builder<Vehiculo>()
                .setQuery(query, Vehiculo.class).build();

        mAdapter = new FirebaseRecyclerAdapter<Vehiculo, VehiculoViewHolder>(options) {
            @Override
                protected void onBindViewHolder(@NonNull VehiculoViewHolder holder, final int position,
                                                @NonNull Vehiculo model) {
                holder.tipo.setText(model.getTipo());
                holder.path_imagen.setText(model.getPath_imagen());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = mRecyclerView.getChildAdapterPosition(view);
                        Toast toast1 = Toast.makeText(getApplicationContext(),
                                "Pulsado el elemento " + pos,
                                Toast.LENGTH_SHORT);
                        toast1.show();
                        Intent intent = new Intent(MainActivity.this, ListaFotosActivity.class);
                        intent.putExtra("@string/tipo_vehiculo", mAdapter.getItem(pos).getTipo());
                        startActivity(intent);

                    }
                });
            }

            @NonNull
            @Override
            public VehiculoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.view_holder_layout, parent, false);
                return new VehiculoViewHolder(view);
            }

        };
        mRecyclerView = findViewById(R.id.my_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }
}

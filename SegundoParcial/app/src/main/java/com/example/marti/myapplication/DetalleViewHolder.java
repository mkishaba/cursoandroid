package com.example.marti.myapplication;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DetalleViewHolder extends RecyclerView.ViewHolder {
    TextView marca;
    TextView modelo;
    ImageView foto;
    ProgressBar downloadProgressBar;

    public DetalleViewHolder(View itemView) {
        super(itemView);
        this.marca = itemView.findViewById(R.id.marcaTxtVw);
        this.modelo = itemView.findViewById(R.id.modeloTxtVw);
        this.foto = itemView.findViewById(R.id.fotoVehiculo);
        this.downloadProgressBar = itemView.findViewById(R.id.progressBar);
    }
}

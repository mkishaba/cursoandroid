package com.example.marti.myapplication;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class VehiculoViewHolder extends RecyclerView.ViewHolder {
    TextView tipo;
    TextView path_imagen;
    public VehiculoViewHolder(View itemView) {
        super(itemView);
        tipo = itemView.findViewById(R.id.tipoTextView);
        path_imagen = itemView.findViewById(R.id.pathTextView);
    }
}

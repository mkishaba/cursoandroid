package com.example.marti.myapplication;

public class Vehiculo {
    private String tipo;
    private String path_imagen;

    public Vehiculo() {}

    public Vehiculo(String tipo, String path_imagen) {
        this.tipo = tipo;
        this.path_imagen = path_imagen;
    }

    public String getTipo() {
        return tipo;
    }

    public String getPath_imagen() {
        return path_imagen;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setPath_imagen(String path_imagen) {
        this.path_imagen = path_imagen;
    }
}

package com.example.marti.myapplication;

public class Detalle {
    private String marca;
    private String modelo;
    private String path_imagen;

    public Detalle() {}

    public Detalle(String marca, String modelo, String path_imagen) {
        this.marca = marca;
        this.modelo = modelo;
        this.path_imagen = path_imagen;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPath_imagen() {
        return path_imagen;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setPath_imagen(String path_imagen) {
        this.path_imagen = path_imagen;
    }
}

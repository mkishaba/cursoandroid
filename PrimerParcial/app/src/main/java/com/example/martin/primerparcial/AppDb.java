package com.example.martin.primerparcial;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AppDb extends SQLiteOpenHelper {
    // Query para crear la base de datos
    private String createTablaUsuarios =
            "CREATE TABLE Usuarios(" +
                    "Usuario TEXT NOT NULL," +
                    "Password TEXT NOT NULL)";

    private String createTablaVehiculos =
            "CREATE TABLE Vehiculos(" +
                "Marca TEXT NOT NULL," +
                "Modelo TEXT NOT NULL," +
                "Tipo TEXT NOT NULL," +
                "PathImagen Text)";

    // Constructor
    public AppDb(Context contexto, String nombre,
                 SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createTablaUsuarios);
        sqLiteDatabase.execSQL(createTablaVehiculos);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Usuarios");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Vehiculos");
        sqLiteDatabase.execSQL(createTablaUsuarios);
        sqLiteDatabase.execSQL(createTablaVehiculos);
    }
}

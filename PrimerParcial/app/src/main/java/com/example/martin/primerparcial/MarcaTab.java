package com.example.martin.primerparcial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;

public class MarcaTab extends Fragment {
  private String mPathImagen;

  public MarcaTab() {}

  public static MarcaTab newInstance() {
    MarcaTab fragment = new MarcaTab();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_marca_tab, container, false);
    ImageView marcaImg = v.findViewById(R.id.marcaImgVw);

    File file = new File(mPathImagen);
    if (file.exists()) {
      String absolutePath = file.getAbsolutePath();
      Bitmap bitmap = BitmapFactory.decodeFile(absolutePath);
      marcaImg.setImageBitmap(bitmap);
    }
    return v;
  }

  public void setPathImagen(String pathImagen) {
    mPathImagen = pathImagen;
  }
}

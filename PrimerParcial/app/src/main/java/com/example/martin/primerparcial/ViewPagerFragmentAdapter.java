package com.example.martin.primerparcial;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class ViewPagerFragmentAdapter extends FragmentPagerAdapter {
  private ArrayList<Fragment> mFragmentList = new ArrayList<>();
  private ArrayList<String>   mTabsTitles = new ArrayList<>();

  public ViewPagerFragmentAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public int getCount() { return mFragmentList.size(); }

  @Override
  public Fragment getItem(int position) { return mFragmentList.get(position); }

  @Nullable
  @Override
  public CharSequence getPageTitle(int position) { return mTabsTitles.get(position); }

  public void addTab(Fragment frg, String title) {
    mFragmentList.add(frg);
    mTabsTitles.add(title);
  }
}

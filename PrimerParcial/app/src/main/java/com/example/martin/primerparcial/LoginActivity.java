package com.example.martin.primerparcial;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {
  private TextView usuario;
  private TextView clave;
  private Intent toMainActivityIntent;
  private CheckBox mRecuerdaUsuario;
  private SQLiteDatabase db;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    Button aceptarBtn;

    // Obtengo los ids de los distintos objetos de la pantalla
    mRecuerdaUsuario = findViewById(R.id.usuarioChkBx);
    usuario = findViewById(R.id.usuarioTextViewId);
    clave = findViewById(R.id.claveTextViewId);
    aceptarBtn = findViewById(R.id.aceptarButtonId);

    final SharedPreferences usuarioSharedPref =
            getSharedPreferences("RECUERDAUSRPASS", Context.MODE_PRIVATE);

    if (usuarioSharedPref.getBoolean("RECUERDAUSRPASS", true)) {
      usuario.setText(usuarioSharedPref.getString("USUARIOSTR", ""));
      clave.setText(usuarioSharedPref.getString("CLAVESTR", ""));
      mRecuerdaUsuario.setChecked(true);
    }

    // Abro la base de datos
    AppDb myDb = new AppDb(this, "myDb", null, 14);
    db = myDb.getWritableDatabase();

    if (db != null) {
      // Seteo foco en el TextView del usuario
      usuario.requestFocus();

      // Cargo el intent a la MainActivity
      toMainActivityIntent = new Intent(this, MainActivity.class);

      // Cargo la tarea del evento del botón "Aceptar"
      aceptarBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        String usuarioStr = usuario.getText().toString();
        String claveStr = clave.getText().toString();

        String[] campos = new String[]{"Usuario", "Password"};
        Cursor c = db.query("Usuarios", campos,
                "Usuario='"+usuarioStr+"'",
                null,null,null,
                null,null);

        // Valido contraseña ingresada con la que esta guardada en la BD
        if (c.moveToFirst()) {
          SharedPreferences.Editor editor = usuarioSharedPref.edit();
          if (mRecuerdaUsuario.isChecked()) {
            editor.putBoolean("RECUERDAUSRPASS", true);
            editor.putString("USUARIOSTR", usuario.getText().toString());
            editor.putString("CLAVESTR", clave.getText().toString());
          } else {
            editor.putBoolean("RECUERDAUSRPASS", false);
          }
          editor.apply();

          if (claveStr.equals(c.getString(1))) {
            toMainActivityIntent.putExtra("USUARIO", usuarioStr);
            startActivity(toMainActivityIntent);
            db.close();
            finish();
          }
        }
        c.close();
        }
      });
    }
  }
}

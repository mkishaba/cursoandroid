package com.example.martin.primerparcial;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
                          implements AgregarFragmentDialog.agregarDialogListener {
  private Intent mToTabsIntent;
  private Intent mToLoginIntent;
  private Intent mToSettingsIntent;
  private ArrayList<Vehiculo> mListadoPrincipal = new ArrayList<>();
  private ListadoVehiculosAdapter mAdapter;
  private ListView mMainList;
  private SQLiteDatabase mDb;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar mainToolbar = findViewById(R.id.mainToolbar);
    setSupportActionBar(mainToolbar);

    mToTabsIntent = new Intent(this, tabsActivity.class);
    mToLoginIntent = new Intent(this, LoginActivity.class);
    mToSettingsIntent = new Intent(this, SettingsActivity.class);

    // Abro la base de datos
    AppDb myDb = new AppDb(this, "myDb", null, 14);
    mDb = myDb.getWritableDatabase();

    // Cargo el vector con los datos almacenados en la base de datos
    if (mDb != null) {
      String[] campos = {"Marca", "Modelo", "Tipo", "PathImagen"};
      Cursor c = mDb.query("Vehiculos", campos, null,null,
              null,null,"Marca",null);

      if (c.moveToFirst()) {
        do {
          mListadoPrincipal.add(new Vehiculo(c.getString(0),
              c.getString(1), c.getString(2), c.getString(3)));
        } while (c.moveToNext());
      }
    }

    mAdapter = new ListadoVehiculosAdapter(this, mListadoPrincipal);

    SharedPreferences fontSize = PreferenceManager
            .getDefaultSharedPreferences(this);
    String tamañoLetra = fontSize.getString("opcion1", "mediano");
    String tipoLetra = fontSize.getString("opcion2", "mediano");
    mAdapter.setListFontSize(tamañoLetra);
    mAdapter.setTipoFuente(tipoLetra);

    mMainList = findViewById(R.id.mainList);
    mMainList.setAdapter(mAdapter);

    mMainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      mToTabsIntent.putExtra("MARCA", mListadoPrincipal.get(position).getMarca());
      mToTabsIntent.putExtra("MODELO", mListadoPrincipal.get(position).getModelo());
      mToTabsIntent.putExtra("TIPOVEHICULO", mListadoPrincipal.get(position).getTipoVehiculo());
      mToTabsIntent.putExtra("PATHIMAGEN", mListadoPrincipal.get(position).getPathImagenVehiculo());
      startActivity(mToTabsIntent);
      }
    });
    registerForContextMenu(mMainList);

    FloatingActionButton agregarBtn = findViewById(R.id.floatingBtn);
    agregarBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AgregarFragmentDialog agregarDialog = new AgregarFragmentDialog();
        agregarDialog.setTitulo(getString(R.string.agregar_vehiculo));
        agregarDialog.setAccion(getString(R.string.agregar_dialog_btn));
        agregarDialog.show(getSupportFragmentManager(), "agregarDialog");
      }
    });
  }

  @Override
  public void onAgregarClick(Vehiculo nuevoVehiculo) {
    if (!(nuevoVehiculo.getMarca().contentEquals("") ||
          nuevoVehiculo.getModelo().contentEquals("")||
          nuevoVehiculo.getTipoVehiculo().contentEquals(""))) {

      //Actualizo primero la base de datos
      if (mDb != null) {
        ContentValues nuevoRegistro = new ContentValues();
        nuevoRegistro.put("Marca", nuevoVehiculo.getMarca());
        nuevoRegistro.put("Modelo", nuevoVehiculo.getModelo());
        nuevoRegistro.put("Tipo", nuevoVehiculo.getTipoVehiculo());
        nuevoRegistro.put("PathImagen", nuevoVehiculo.getPathImagenVehiculo());
        mDb.insert("Vehiculos", null, nuevoRegistro);
      }
      mListadoPrincipal.add(nuevoVehiculo);
      mAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public void onActualizarClick(Vehiculo nuevoVehiculo, int position) {
    if (!(nuevoVehiculo.getMarca().contentEquals("") ||
              nuevoVehiculo.getModelo().contentEquals("")||
              nuevoVehiculo.getTipoVehiculo().contentEquals(""))) {

      //Actualizo primero la base de datos
      if (mDb != null) {
        ContentValues nuevoRegistro = new ContentValues();
        nuevoRegistro.put("Marca", nuevoVehiculo.getMarca());
        nuevoRegistro.put("Modelo", nuevoVehiculo.getModelo());
        nuevoRegistro.put("Tipo", nuevoVehiculo.getTipoVehiculo());
        nuevoRegistro.put("PathImagen", nuevoVehiculo.getPathImagenVehiculo());

        String whereStr = "Marca='"+mListadoPrincipal.get(position).getMarca()+"' AND "+
                          "Modelo='"+mListadoPrincipal.get(position).getModelo()+"' AND "+
                          "Tipo='"+mListadoPrincipal.get(position).getTipoVehiculo()+"'";
        mDb.update("Vehiculos", nuevoRegistro, whereStr, null);
      }
      mListadoPrincipal.get(position).setMarca(nuevoVehiculo.getMarca());
      mListadoPrincipal.get(position).setModelo(nuevoVehiculo.getModelo());
      mListadoPrincipal.get(position).setTipoVehiculo(nuevoVehiculo.getTipoVehiculo());
      mListadoPrincipal.get(position).setPathImagenVehiculo(nuevoVehiculo.getPathImagenVehiculo());
      mAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    getMenuInflater().inflate(R.menu.list_context_menu, menu);
    super.onCreateContextMenu(menu, v, menuInfo);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
       case R.id.Settings:
        startActivityForResult(mToSettingsIntent, 1);
        break;
      case R.id.Version:
        AlertDialog.Builder versionOptBuilder = new AlertDialog.Builder(this);
        versionOptBuilder.setTitle("Versión de Software");
        versionOptBuilder.setMessage("1er Parcial Dispositivos Móviles\n\n"+"Versión "
                                  + getString(R.string.version_software));
        versionOptBuilder.setPositiveButton("OK",null);
        versionOptBuilder.create();
        versionOptBuilder.show();
        break;
      case R.id.cerrar_sesion:
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Desea cerrar sesión?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            mDb.close();
            startActivity(mToLoginIntent);
            finish();
          }
        });
        builder.setNegativeButton("Cancelar",null);
        builder.create();
        builder.show();
        break;
      default:
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == 1) {
      if (resultCode == RESULT_CANCELED) {
      } else {
        SharedPreferences fontPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        String tamañoLetra = fontPreferences.getString("opcion1", "mediano");
        String tipoLetra = fontPreferences.getString("opcion2", "Sans");
        mAdapter.setListFontSize(tamañoLetra);
        mAdapter.setTipoFuente(tipoLetra);
        mAdapter.notifyDataSetChanged();
      }
    }
  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {

    AdapterView.AdapterContextMenuInfo info=(AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

    switch(item.getItemId()) {
      case R.id.actualizarItem:
        AgregarFragmentDialog agregarDialog = new AgregarFragmentDialog();
        agregarDialog.setTitulo(getString(R.string.actualizar_vehiculo));
        agregarDialog.setAccion(getString(R.string.actualizar_dialog_btn));

        agregarDialog.setMarcaOld(mListadoPrincipal.get(info.position).getMarca());
        agregarDialog.setModeloOld(mListadoPrincipal.get(info.position).getModelo());
        agregarDialog.setTipoVehiculoOld(mListadoPrincipal.get(info.position).getTipoVehiculo());

        agregarDialog.setItemPosition(info.position);
        agregarDialog.show(getSupportFragmentManager(), "actualizarDialog");
        break;
      case R.id.borrarItem:
        //Actualizo primero la base de datos
        if (mDb != null) {
          ContentValues nuevoRegistro = new ContentValues();
          String whereStr ="Marca='"+mListadoPrincipal.get(info.position).getMarca()+"' AND "+
                           "Modelo='"+mListadoPrincipal.get(info.position).getModelo()+"' AND "+
                           "Tipo='"+mListadoPrincipal.get(info.position).getTipoVehiculo()+"'";

          mDb.delete("Vehiculos", whereStr, null);
        }
        mListadoPrincipal.remove(info.position);
        mAdapter.notifyDataSetChanged();
      break;
    }
    return super.onContextItemSelected(item);
  }
}

package com.example.martin.primerparcial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;

public class ListadoVehiculosAdapter extends ArrayAdapter<Vehiculo> {
  private ArrayList<Vehiculo> mDatos;
  private int mMarcaFontSize = 20;
  private int mModeloFontSize = 15;
  private Typeface mTipoFuente = Typeface.DEFAULT;

  public ListadoVehiculosAdapter(Context context, ArrayList<Vehiculo> datos) {
    super(context, R.layout.vehiculos_list, datos);
    mDatos = datos;
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    LayoutInflater inflater = LayoutInflater.from(getContext());
    View item = inflater.inflate(R.layout.vehiculos_list, null);

    TextView marca = item.findViewById(R.id.marcaTextView);
    TextView modelo = item.findViewById(R.id.modeloTextView);
    ImageView imagen = item.findViewById(R.id.imagenLista);

    marca.setText(mDatos.get(position).getMarca());
    modelo.setText(mDatos.get(position).getModelo());

    marca.setTextSize(TypedValue.COMPLEX_UNIT_DIP, mMarcaFontSize);
    modelo.setTextSize(TypedValue.COMPLEX_UNIT_DIP, mModeloFontSize);

    marca.setTypeface(mTipoFuente);
    modelo.setTypeface(mTipoFuente);

    if (mDatos.get(position).getPathImagenVehiculo().contentEquals("")) {
      if (mDatos.get(position).getTipoVehiculo().toLowerCase().contains("auto")) {
        imagen.setImageResource(R.drawable.icono_auto);
      }

      if (mDatos.get(position).getTipoVehiculo().toLowerCase().contains("moto")) {
        imagen.setImageResource(R.drawable.moto);
      }
    } else {
      File file = new File(mDatos.get(position).getPathImagenVehiculo());
      if (file.exists()) {
          String absolutePath = file.getAbsolutePath();
          Bitmap bitmap = BitmapFactory.decodeFile(absolutePath);
          imagen.setImageBitmap(bitmap);
      }
    }
    return item;
  }

  public void setListFontSize(String tamaño) {
    switch(tamaño) {
      case "pequeño":
          setMarcaFontSize(15);
          setModeloFontSize(10);
          break;
      case "mediano":
          setMarcaFontSize(20);
          setModeloFontSize(15);
          break;
      case "grande":
          setMarcaFontSize(25);
          setModeloFontSize(20);
          break;
      default:
    }
  }

  public void setTipoFuente(String tipoFuente) {
    switch (tipoFuente) {
      case "Sans":
        mTipoFuente = Typeface.SANS_SERIF;
        break;
      case "Serif":
        mTipoFuente = Typeface.SERIF;
        break;
      case "Monospace":
        mTipoFuente = Typeface.MONOSPACE;
        break;
    }
  }

  public void setMarcaFontSize(int marcaFontSize) {
    this.mMarcaFontSize = marcaFontSize;
  }
  public void setModeloFontSize(int modeloFontSize) {
    this.mModeloFontSize = modeloFontSize;
  }
}

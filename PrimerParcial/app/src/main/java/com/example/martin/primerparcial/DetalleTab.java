package com.example.martin.primerparcial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetalleTab extends Fragment {
  private String mMarca="";
  private String mModelo="";
  private String mTipo="";

  public DetalleTab() {}

  public static DetalleTab newInstance() {
    DetalleTab fragment = new DetalleTab();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    View v = inflater.inflate(R.layout.fragment_detalle_tab, container, false);
    TextView marca = v.findViewById(R.id.MarcaTxtVw);
    TextView modelo = v.findViewById(R.id.ModeloTxtVw);
    TextView tipo = v.findViewById(R.id.TipoTxtVw);
    marca.setText(mMarca);
    modelo.setText(mModelo);
    tipo.setText(mTipo);
    return v;
  }

  public void setMarca(String marca) {
    this.mMarca = marca;
  }

  public void setModelo(String modelo) {
    this.mModelo = modelo;
  }

  public void setTipo(String tipo) {
    this.mTipo = tipo;
  }
}

package com.example.martin.primerparcial;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class tabsActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tabs);

    Intent intent = getIntent();

    TabLayout myTabs = findViewById(R.id.myTabs);
    ViewPager myViewPager = findViewById(R.id.myViewPager);
    ViewPagerFragmentAdapter adapter =
            new ViewPagerFragmentAdapter(getSupportFragmentManager());

    DetalleTab detalleTab = new DetalleTab();
    detalleTab.setMarca("Marca: "+intent.getStringExtra("MARCA"));
    detalleTab.setModelo("Modelo: "+intent.getStringExtra("MODELO"));
    detalleTab.setTipo("Tipo: "+intent.getStringExtra("TIPOVEHICULO"));
    adapter.addTab(detalleTab, "Detalle");

    MarcaTab marcaTab = new MarcaTab();
    marcaTab.setPathImagen(intent.getStringExtra("PATHIMAGEN"));
    adapter.addTab(marcaTab, "Foto");
    //adapter.addTab(new ModeloTab(), "Modelo");

    myViewPager.setAdapter(adapter);
    myTabs.setupWithViewPager(myViewPager);
  }
}

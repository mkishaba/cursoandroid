package com.example.martin.primerparcial;

public class Vehiculo {
  private String mMarca;
  private String mModelo;
  private String mTipoVehiculo;
  private String mPathImagenVehiculo;

  public Vehiculo(){}

  public Vehiculo(String marca, String modelo, String tipoVehiculo, String path) {
    mMarca = marca;
    mTipoVehiculo = tipoVehiculo;
    mModelo = modelo;
    mPathImagenVehiculo = path;
  }

  public String getModelo() {
    return mModelo;
  }

  public String getMarca() {
    return mMarca;
  }

  public String getTipoVehiculo() {
    return mTipoVehiculo;
  }

  public String getPathImagenVehiculo() { return mPathImagenVehiculo; }

  public void setModelo(String modelo) { this.mModelo = modelo; }

  public void setMarca(String marca) { this.mMarca = marca; }

  public void setTipoVehiculo(String tipoVehiculo) { this.mTipoVehiculo = tipoVehiculo; }

  public void setPathImagenVehiculo(String pathImagenVehiculo) {
      this.mPathImagenVehiculo = pathImagenVehiculo;
  }
}
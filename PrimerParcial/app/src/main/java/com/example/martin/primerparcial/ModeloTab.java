package com.example.martin.primerparcial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ModeloTab extends Fragment {
  public ModeloTab() {
    // Required empty public constructor
  }

  public static ModeloTab newInstance() {
    ModeloTab fragment = new ModeloTab();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_modelo_tab, container, false);
  }
}

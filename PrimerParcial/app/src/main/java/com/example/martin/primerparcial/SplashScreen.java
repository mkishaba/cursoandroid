package com.example.martin.primerparcial;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {
  private Intent loginIntent;            // Intent a la pantalla del login

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash_screen);

    String flagPrimerEjecucionStrId = "PrimerEjecucion";
    int splashDelay = 1000;           // Tiempo de visualización del splash screen

    // Cargo el intent al login
    loginIntent = new Intent(this, LoginActivity.class);

    // Pantalla con orientación vertical
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    // Creo una tarea temporizada
    TimerTask splashTask = new TimerTask() {
      @Override
      public void run() {
        startActivity(loginIntent);
        finish();
      }
    };

    // Creo el timer para ejecutar la tarea temporizada
    Timer splashTimer = new Timer();

    SharedPreferences flagPrimerEjecucion =
            getSharedPreferences(flagPrimerEjecucionStrId, Context.MODE_PRIVATE);
    Boolean primerEjecucion =
            flagPrimerEjecucion.getBoolean(flagPrimerEjecucionStrId, true);

    // Si es la primer ejecución de la aplicación, creo la base de datos
    if (primerEjecucion) {
      // Me conecto o creo la base de datos
      AppDb myDb = new AppDb(this, "myDb", null, 14);
      SQLiteDatabase db = myDb.getWritableDatabase();

      db.execSQL("DROP TABLE IF EXISTS Usuarios");

      db.execSQL("CREATE TABLE Usuarios(" +
              "Usuario TEXT NOT NULL," +
              "Password TEXT NOT NULL)");

      db.execSQL("DROP TABLE IF EXISTS Vehiculos");

      db.execSQL("CREATE TABLE Vehiculos(" +
              "Marca TEXT NOT NULL," +
              "Modelo TEXT NOT NULL," +
              "Tipo TEXT NOT NULL,"+
              "PathImagen TEXT)");

      // Creo el usuario "admin"
      db.execSQL("INSERT INTO Usuarios(Usuario, Password)" +
              "VALUES('admin', '1234')");

      db.execSQL("INSERT INTO Vehiculos(Marca, Modelo, Tipo, PathImagen)" +
              "VALUES('Ford', 'Focus', 'Automóvil', '')");

      db.execSQL("INSERT INTO Vehiculos(Marca, Modelo, Tipo, PathImagen)" +
              "VALUES('Renault', 'Clio', 'Automóvil', '')");

      db.execSQL("INSERT INTO Vehiculos(Marca, Modelo, Tipo, PathImagen)" +
              "VALUES('Peugeot', '208', 'Automóvil', '')");

      db.execSQL("INSERT INTO Vehiculos(Marca, Modelo, Tipo, PathImagen)" +
              "VALUES('Chevrolet', 'Corsa', 'Automóvil', '')");

      db.execSQL("INSERT INTO Vehiculos(Marca, Modelo, Tipo, PathImagen)" +
              "VALUES('Citroen', 'C4', 'Automóvil', '')");

      SharedPreferences.Editor editor = flagPrimerEjecucion.edit();
      editor.putBoolean(flagPrimerEjecucionStrId, false);
      editor.commit();
    }
    splashTimer.schedule(splashTask, splashDelay);
  }
}

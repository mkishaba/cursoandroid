package com.example.martin.primerparcial;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;

import static android.app.Activity.RESULT_OK;
import static android.graphics.BitmapFactory.decodeFile;
import static android.graphics.BitmapFactory.decodeStream;

public class AgregarFragmentDialog extends DialogFragment {

  private EditText mMarca;
  private EditText mModelo;
  private RadioGroup mTipoVehiculo;
  private RadioButton mTipoAutoChkBtn;
  private RadioButton mTipoMotoChkBtn;
  private String mTitulo;
  private String mAccion;
  private String mMarcaOld = "";
  private String mModeloOld = "";
  private String mTipoVehiculoOld = "";
  private String mPathImagen = "";
  private Button mAgregarFotoBtn;
  private int mItemPosition = 0;
  private agregarDialogListener mListener;
  private ImageView pruebaIMG;

  public interface agregarDialogListener {
    void onAgregarClick(Vehiculo nuevoVehiculo);
    void onActualizarClick(Vehiculo nuevoVehiculo, int position);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    try {
      mListener = (agregarDialogListener) context;
    } catch (ClassCastException e) {
      throw new ClassCastException(context.toString() +
              " se debe implementar agregarDialogListener");
    }
  }

  public AgregarFragmentDialog(){}

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = getActivity().getLayoutInflater();
    View view = inflater.inflate(R.layout.agregar_dialog, null);

    builder.setView(view)
    .setPositiveButton(mAccion, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        Vehiculo nuevoVehiculo = new Vehiculo();
        nuevoVehiculo.setMarca(mMarca.getText().toString());
        nuevoVehiculo.setModelo(mModelo.getText().toString());
        nuevoVehiculo.setPathImagenVehiculo(mPathImagen);

        nuevoVehiculo.setTipoVehiculo("Automóvil");
        if (mTipoAutoChkBtn.isChecked()) {
          nuevoVehiculo.setTipoVehiculo("Automóvil");
        }

        if (mTipoMotoChkBtn.isChecked()) {
          nuevoVehiculo.setTipoVehiculo("Moto");
        }

        if (mAccion.contentEquals(getString(R.string.agregar_dialog_btn))) {
          mListener.onAgregarClick(nuevoVehiculo);
        }
        if (mAccion.contentEquals(getString(R.string.actualizar_dialog_btn))) {
          mListener.onActualizarClick(nuevoVehiculo, mItemPosition);
        }
      }
    })
    .setNegativeButton(getString(R.string.cancelar_dialog_btn),
          new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {}
    });

    pruebaIMG = view.findViewById(R.id.pruebaIMG);

    TextView tituloDialogo = view.findViewById(R.id.tituloDialogo);
    mMarca = view.findViewById(R.id.marcaEdtTxt);
    mModelo = view.findViewById(R.id.modeloEdtTxt);
    mTipoVehiculo = view.findViewById(R.id.tipoVehiculoRadioGroup);
    mTipoAutoChkBtn = view.findViewById(R.id.autoRadioButton);
    mAgregarFotoBtn = view.findViewById(R.id.agregarFotoButton);
    mTipoMotoChkBtn = view.findViewById(R.id.motoRadioButton);
    mTipoAutoChkBtn.toggle();

    if (!mMarcaOld.isEmpty()) { mMarca.setText(mMarcaOld); }
    if (!mModeloOld.isEmpty()) { mModelo.setText(mModeloOld); }
    if (!mTipoVehiculoOld.isEmpty()) {
      if (mTipoVehiculoOld.toLowerCase().contains("moto")) {
        mTipoMotoChkBtn.toggle();
      } else {
        mTipoAutoChkBtn.toggle();
      }
    }

    mAgregarFotoBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivityForResult( new Intent(Intent.ACTION_PICK,
          android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI),
         1);
      }
    });
    tituloDialogo.setText(mTitulo);
    return builder.create();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if(resultCode==RESULT_OK && requestCode==1) {
      RealPathUtil realPathUtil = new RealPathUtil();
      Uri selectedImage = data.getData();
      mPathImagen = realPathUtil.getRealPath(getContext(), selectedImage);
      File file = new File(mPathImagen);
      if (file.exists()) {
        String absolutePath = file.getAbsolutePath();
        Bitmap bitmap = BitmapFactory.decodeFile(absolutePath);
        pruebaIMG.setImageBitmap(bitmap);
      }
    }
  }

  public void setTitulo(String titulo) {
    this.mTitulo = titulo;
  }

  public void setAccion(String accion) {
    this.mAccion = accion;
  }

  public void setMarcaOld(String marcaOld) {
    this.mMarcaOld = marcaOld;
  }

  public void setModeloOld(String modeloOld) {
    this.mModeloOld = modeloOld;
  }

  public void setTipoVehiculoOld(String tipoVehiculoOld) {
    this.mTipoVehiculoOld = tipoVehiculoOld;
  }

  public void setItemPosition(int itemPosition) {
    this.mItemPosition = itemPosition;
  }
}

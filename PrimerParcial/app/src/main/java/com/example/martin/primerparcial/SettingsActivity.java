package com.example.martin.primerparcial;

import android.content.Intent;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;
import android.os.Bundle;

public class SettingsActivity extends PreferenceActivity {
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.settings);
    Intent i = getIntent();
    setResult(RESULT_OK, i);
  }
}
